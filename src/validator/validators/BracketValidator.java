package validator.validators;

import lex.AnalyzerSyntaxException;
import lex.Token;
import validator.Validator;

import java.util.List;

import static sintax.Limiters.LEFT_BRACKET;
import static sintax.Limiters.RIGHT_BRACKET;

/**
 * проверяет кол-во скобок
 */
public class BracketValidator implements Validator {
    @Override
    public void validate(List<Token> tokens) {
        int count = 0;
        int position = 0;
        for (Token token : tokens) {
            position++;
            if (RIGHT_BRACKET.symbol.equals(token.getLexem().toString())) {
                count++;
            }
            if (LEFT_BRACKET.symbol.equals(token.getLexem().toString())) {
                count--;
            }
        }
        if (count != 0) {
            throw new AnalyzerSyntaxException(" SyntaxError: Unexpected token )", position, tokens);
        }
    }
}
