package validator;

import lex.Token;

import java.util.List;

public interface Validator {

    void validate(List<Token> tokens);
}
