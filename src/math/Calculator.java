package math;


import lex.Expression;
import lex.Token;

import java.util.List;

public class Calculator {


    public double calculate(Expression expression) {
        List<Token> tokens = expression.getTokens();
        List<Expression> expressions = expression.getExpressions();
        List<Token> operations = expression.getOperations();
        Double result = null;
        if (tokens != null && !tokens.isEmpty()) {
            return Double.parseDouble(tokens.get(0).getLexem().toString());
        }

        if (expressions != null && !expressions.isEmpty()) {
            result = expressions.get(0).calculate();
            for (int i = 0; i < expressions.size() - 1; i++) {
                if (expressions.size() >= i + 1) {
                    result = evaluate(result, expressions.get(i + 1).calculate(), operations.get(i).getLexem().toString());
                }
            }
        }
        return result;
    }

    public Double evaluate(Double a, Double b, String oper) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        if ("+".equals(oper)) {
            return a + b;
        }

        if ("-".equals(oper)) {
            return a - b;
        }

        if ("*".equals(oper)) {
            return a * b;
        }

        if ("/".equals(oper)) {
            return a / b;
        }
        return null;
    }

}
