package lex;

import math.Calculator;

import java.util.ArrayList;
import java.util.List;

import static lex.Patterns.isDigital;

/**
 * Выражение
 */
public class Expression {
    /**
     * список вложеных выражений
     */
    private List<Expression> expressions;
    /**
     * Лексемы данного выражения
     */
    private List<Token> tokens;

    private List<Token> operations;
    private Error error;

    public double calculate() {
        Calculator calculator = new Calculator();
        return calculator.calculate(this);
    }

    public void analyze() {
        analyzeCurrent();
        if (!getExpressions().isEmpty()) {
            getExpressions().stream().filter(expression -> expression.getTokens().size() > 1).forEach(Expression::analyze);
        }

    }

    private void analyzeCurrent() {
        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(0);
            if (isDigital(token.getLexem())) {
                Expression expression = new Expression();
                expression.getTokens().add(token);
                getExpressions().add(expression);
                tokens.remove(0);
                i--;
                continue;
            }
            if ("+-*/".contains(token.getLexem())) {
                getOperations().add(token);
                i--;
                tokens.remove(0);
            }
            if (")".contains(token.getLexem())) {
                throw new AnalyzerSyntaxException(" SyntaxError: Unexpected token: " + token.getLexem(), i, tokens);
            }

            if ("(".contains(token.getLexem())) {
                getExpressions().add(getExpressionInBrackets());
                i--;
            }
        }
    }

    private Expression getExpressionInBrackets() {
        int countBreacket = 1;
        tokens.remove(0);
        Expression expression = new Expression();
        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(0);
            if ("(".contains(token.getLexem())) {
                countBreacket++;
            }
            if (")".contains(token.getLexem())) {
                countBreacket--;
            }
            if (countBreacket == 0) {
                tokens.remove(0);
                i--;
                break;
            }
            expression.getTokens().add(token);
            tokens.remove(0);
            i--;
        }
        return expression;
    }


    public void addLexeme(Token token) {
        if (this.tokens == null) {
            this.tokens = new ArrayList<>();
        }
        if (token != null) {
            tokens.add(token);
        }
    }

    public List<Expression> getExpressions() {
        if (expressions == null) {
            expressions = new ArrayList<>();
        }
        return expressions;
    }

    public void setExpressions(List<Expression> expressions) {
        this.expressions = expressions;
    }

    public List<Token> getTokens() {
        if (tokens == null) {
            tokens = new ArrayList<>();
        }
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error errors) {
        this.error = errors;
    }

    public List<Token> getOperations() {
        if (operations == null) {
            operations = new ArrayList<>();
        }
        return operations;
    }

    public void setOperations(List<Token> operations) {
        this.operations = operations;
    }
}
