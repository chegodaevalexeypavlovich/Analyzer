package lex;

public class Token {
    private StringBuilder lexem;
    private Type type;


    public Token(StringBuilder lexem, Type type) {
        this.lexem = lexem;
        this.type = type;
    }

    public StringBuilder getLexem() {
        return lexem;
    }

    public void setLexem(StringBuilder lexem) {
        this.lexem = lexem;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
