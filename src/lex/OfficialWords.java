package lex;

import sintax.Limiters;

public enum OfficialWords {
    SUBTRACTION("-"),
    ASSIGNMENT("="),
    PROGRAM("Program"),
    VAR("var"),
    BEGIN("Begin"),
    END("End");

    public String symbol;

    OfficialWords(String symbol) {
        this.symbol = symbol;
    }

    public static OfficialWords getLimiter(String s) {
        for (OfficialWords words : OfficialWords.values()) {
            if (words.symbol.equals(s)) {
                return words;
            }
        }
        return null;
    }

    public static boolean contains(String s) {
        for (OfficialWords words : OfficialWords.values()) {
            if (words.symbol.equals(s)) {
                return true;
            }
        }
        return false;
    }
}
