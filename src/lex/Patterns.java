package lex;

import java.util.regex.Pattern;


public class Patterns {
    public static final Pattern numberPattern = Pattern.compile("^[0-9]*$", Pattern.CASE_INSENSITIVE);
    public static final Pattern cpetialCharPattern = Pattern.compile("^[()-+*/.]$", Pattern.CASE_INSENSITIVE);
    public static final Pattern ignore = Pattern.compile("^[ #]$", Pattern.CASE_INSENSITIVE);
    public static final Pattern chars = Pattern.compile("^[A-Za-z]*$", Pattern.CASE_INSENSITIVE);

    public static boolean isDigital(StringBuilder s) {
        return numberPattern.matcher(s).matches();
    }

    public static boolean isChar(StringBuilder s) {
        return chars.matcher(s).matches();
    }

}
