package lex;

import java.util.List;

public class AnalyzerSyntaxException extends RuntimeException {
    private int position;
    private List<Token> tokens;
    private StringBuilder s;

    public AnalyzerSyntaxException(String message, int position, StringBuilder s) {
        super(message);
        this.position = position;
        this.s = s;
    }

    public AnalyzerSyntaxException(String message, int position, List<Token> tokens) {
        super(message);
        this.position = position;
        this.tokens = tokens;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public void printStackTrace() {
        for (Token token : tokens) {
            System.out.print(token.getLexem());
        }
        System.out.println();
        for (int i = 0; i < position; i++) {
            System.out.print(" ");
        }
        System.out.print("↑");
        System.out.println(getMessage() + " " + position);
    }
}
