package lex;

public enum Type {
    LEX,
    LIMITER,
    NUM,
    OFFICIAL_WORD,
    IDENTIFIER;
}
