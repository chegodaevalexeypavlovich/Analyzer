package lex;


import validator.validators.BracketValidator;
import validator.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static lex.Patterns.*;

public class LexAnalyzer {
    private List<Token> tokens = new ArrayList<>();

    public List<Token> analyze(StringBuilder s) {
        validateParams(s);
        parseTokens(s);
        validateTokens(tokens);
        return tokens;
    }

    /**
     * парсит выражение на токены
     *
     * @param s строка для парсинга
     */
    private void parseTokens(StringBuilder s) {
        while (s.length() != 0) {
            StringBuilder nextSubstring = getNextSubstring(s);
            if (isDigital(nextSubstring)) {
                s = addTokenByPattern(s, nextSubstring, numberPattern);
            } else {
                if (isChar(nextSubstring)) {
                    s = addTokenByPattern(s, nextSubstring, chars);
                } else {
                    addNewToken(analyzeToken(nextSubstring));
                }
            }
        }
    }

    private void addSpetialWordToken() {

    }

    /**
     * добавляет цифровой токен
     *
     * @param s             исходная строка
     * @param nextSubstring первый найденый цифровой символ
     * @return исхоную строку с удаленными из нее символами
     */
    private StringBuilder addTokenByPattern(StringBuilder s, StringBuilder nextSubstring, Pattern pattern) {
        StringBuilder append = nextSubstring.append(s);
        addNewToken(analyzeToken(getNextSubstringByPattern(append, pattern)));
        s = append;
        return s;
    }

    private void addNewToken(Token token) {
        if (token != null && token.getLexem().length() > 0) {
            tokens.add(token);
        }
    }

    /**
     * ищет первое попавшееся в строке выражение по заданному патеррну
     *
     * @param s строка в которой происходит поиск
     * @return лексему с этим числом
     */
    private StringBuilder getNextSubstringByPattern(StringBuilder s, Pattern pattern) {
        StringBuilder nextSubstring = getNextSubstring(s);
        StringBuilder digitalPart = new StringBuilder();
        while (nextSubstring != null && pattern.matcher(nextSubstring).matches()) {
            digitalPart.append(nextSubstring);
            nextSubstring = getNextSubstring(s);
        }
        s.insert(0, nextSubstring);
        return digitalPart;
    }

    /**
     * Возвращает 1 символ из сроки удаляя его из исходной строки
     *
     * @param s
     * @return
     */
    private StringBuilder getNextSubstring(StringBuilder s) {
        if (s.length() > 0) {
            StringBuilder stringBuilder = new StringBuilder(s.substring(0, 1));
            s.deleteCharAt(0);
            return stringBuilder;
        }
        return null;
    }


    private void validateParams(StringBuilder s) {
        int i = s.length() - 1;
        if (s.lastIndexOf("#") != i) {
            throw new AnalyzerSyntaxException("Ожидался символ #", s.length(), s);
        }
    }

    private Token analyzeToken(StringBuilder s) {
        if (numberPattern.matcher(s).matches()) {
            return new Token(s, Type.NUM);
        }
        if (cpetialCharPattern.matcher(s).matches()) {
            return new Token(s, Type.LIMITER);
        }
        if (chars.matcher(s).matches() && OfficialWords.contains(s.toString())) {
            return new Token(s, Type.OFFICIAL_WORD);
        }
        if (chars.matcher(s).matches() && !OfficialWords.contains(s.toString())) {
            return new Token(s, Type.IDENTIFIER);
        }
        if (ignore.matcher(s).matches()) {
            return null;
        }
        throw new AnalyzerSyntaxException(" SyntaxError: Unexpected token " + s, 0, s);
    }

    private void validateTokens(List<Token> tokens) {
        for (Validator validator : getValidators()) {
            validator.validate(tokens);
        }
    }

    private List<Validator> getValidators() {
        List<Validator> validators = new ArrayList<>();
        validators.add(new BracketValidator());
        return validators;
    }

}
