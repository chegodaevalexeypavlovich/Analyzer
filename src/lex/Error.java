package lex;

public class Error {
    private ErrorType type;
    private String message = "Ошибка";
    private int column;

    public Error(ErrorType type, String message, int column) {
        this.type = type;
        this.message = message;
        this.column = column;
    }

    public ErrorType getType() {
        return type;
    }

    public void setType(ErrorType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
