import lex.Error;
import lex.Expression;
import lex.LexAnalyzer;
import lex.Token;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        LexAnalyzer lexAnalyzer = new LexAnalyzer();
        String expression = scanner.nextLine();

        List<Token> tokenList = lexAnalyzer.analyze(new StringBuilder(expression));
        Expression analyzeExpression = new Expression();
        analyzeExpression.setTokens(tokenList);
        analyzeExpression.analyze();
        System.out.println(analyzeExpression.calculate());
    }

    private static void printErrorIfExist(Expression expression, String exp) {
        Error error = expression.getError();
        if (error != null) {
            System.out.println(exp);
            for (int j = 0; j < error.getColumn(); j++) {
                System.out.print(" ");
            }
            System.out.println("↑");
            System.out.println(error.getMessage() + " " + error.getType() + " " + error.getColumn());
        }
    }
}



