package sintax;


public enum Limiters {

    RIGHT_BRACKET(")"),
    LEFT_BRACKET("("),
    MULTIPLICATION("*"),
    DIVISION("/"),
    ADDITION("+"),
    SUBTRACTION("-"),
    ASSIGNMENT("=");

    public String symbol;

    Limiters(String symbol) {
        this.symbol = symbol;
    }

    public static Limiters getLimiter(String s) {
        for (Limiters limiters : Limiters.values()) {
            if (limiters.symbol.equals(s)) {
                return limiters;
            }
        }
        return null;
    }

    public static boolean contains(String s) {
        for (Limiters limiters : Limiters.values()) {
            if (limiters.symbol.equals(s)) {
                return true;
            }
        }
        return false;
    }
}
