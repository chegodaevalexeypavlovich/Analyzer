package sintax;


import lex.Token;

import java.util.List;

public interface Analyzer {

    boolean analyze(List<Token> tokens);
}
